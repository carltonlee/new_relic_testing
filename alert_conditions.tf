data "newrelic_entity" "frontend-app" {
  name   = "${var.tenant_name}-app"
  type   = "APPLICATION"
  domain = "APM"
}
resource "newrelic_alert_policy" "client_error" {
  name = "Client Error Rate"
}

resource "newrelic_alert_condition" "client_error" {
  policy_id = newrelic_alert_policy.client_error.id

  name            = "Client Error Rate"
  type            = "browser_metric"
  entities        = [data.newrelic_entity.frontend-app.application_id]
  metric          = "page_views_with_js_errors"
  condition_scope = "application"

  term {
    duration      = 5
    operator      = "above"
    priority      = "warning"
    threshold     = "5"
    time_function = "all"
  }

  term {
    duration      = 10
    operator      = "above"
    priority      = "critical"
    threshold     = "5"
    time_function = "all"
  }
}

resource "newrelic_alert_policy_channel" "slack_send" {
  channel_ids = [newrelic_alert_channel.slack.id]
  policy_id   = newrelic_alert_policy.client_error.id
}
