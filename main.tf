resource "newrelic_alert_channel" "slack" {
  name = "${var.tenant_name}-Slack-NewRelic"
  type = "slack"

  config {
    url     = var.slack_url
    channel = var.slack_channel
  }
}