output "slack_channel" {
  value = newrelic_alert_channel.slack.name
}

output "alert_policy" {
  value = newrelic_alert_policy.client_error.name
}


