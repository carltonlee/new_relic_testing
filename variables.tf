variable "api_key" {}

variable "new_relic_account_id" {}

variable "slack_channel" {}

variable "slack_url" {}

variable "tenant_name" {}
