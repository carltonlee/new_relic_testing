provider "newrelic" {
  api_key    = var.api_key
  account_id = var.new_relic_account_id
  region     = "US"
}

terraform {
  backend "s3" {
    bucket               = "fv-global-fv-tf-backend"
    key                  = "newrelic"
    dynamodb_table       = "fv-global-fv-tf-backend-table"
    region               = "us-west-2"
    encrypt              = true
    workspace_key_prefix = "filevine-newrelic-alerting"
  }
}